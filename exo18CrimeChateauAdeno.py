
#Tout d'abord, on établit un dictionnaire qui liste les suspects avec leur ADN:

suspects = {
            'Colonel Moutarde' : "CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATGCAGGAAGCGGCAGGAATAAGGAAAAGCAGCADN",
            'Mlle ROSE' : "CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGGADN",
            'Mme PERVENCHE' : "AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCCADN",
            'M.LEBLANC' : "CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG"
            }

#Ensuite, on déclare les 2 fragments ADN du coupable:

adnCoupable1 = "CATA"
adnCoupable2 = "ATGC"

#Puis, on déclare la variable qui permet de dissocier les 2 suspects (colonel Moutarde et Mlle Rose) ayant les 2 fragments dans leur ADN.

fragmentAdnCoupableAssocies = "CATATGC"
#Cette variable 'fragmentAdnCoupableAssocies' est importante pour choisir le vrai coupable.
#On recherche 2 fragments d'ADN, provenant de deux positions éloignées.
#On doit donc sélectionner un suspect ayant 2 fragments bien éloignés l'un de l'autre et non associés sous la forme de CATATGC.

for coupable in suspects: #boucle for qui parcourt le dictionnaire suspects

    if adnCoupable1 in suspects[coupable] and adnCoupable2 in suspects[coupable] and fragmentAdnCoupableAssocies not in suspects[coupable]:
    #Si l'ADN possède bien les 2 fragments et qu'il ne contient pas CATATGC, alors on affiche le suspect qui devient le coupable:

            print("Le coupable est " + coupable + " car elle possède les fragments ADN '" + adnCoupable1 + "' et '" + adnCoupable2 + "' qui sont bien éloignés l'un de l'autre.")