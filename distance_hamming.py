
#On récupère les mots dont on souhaite mesurer la distance de hamming:

motA = input("Veuillez saisir un premier mot: ")
motB = input("Veuillez saisir un deuxième mot de même longueur que le premier choisi précédemment: ")

#Conversion des mots choisis en majuscule avec la fonction upper(). De cette façon, on évite les erreurs liées à la casse:

mot1 = motA.upper()
mot2 = motB.upper()

#Déclaration de la fonction distance_hamming():

def distance_hamming(mot1,mot2): #ajout en paramètres de la fonction distance_hamming(), les 2 variables contenant les mots saisis par l'utilisateur (mot1,mot2)
    
    if len(mot1) == len(mot2): #Si la longueur du mot1 est strictement égale à celle du mot2, alors on calcule la distance de hamming en effectuant le code ci-dessous:

        distanceH = 0 #on commence avec un compteur à 0

        for lettre in range (0, len(mot1)): #boucle for permettant de vérifier lettre par lettre les 2 mots en fonction de leur longueur

            if mot1[lettre] != mot2[lettre]: 

                distanceH = distanceH + 1  #Si la lettre du mot1 est différente de celle du mot2 alors on incrémente de 1 la variable distanceH (qui correspond à la distance de Hamming)
                
        return distanceH
    
    else:
        
        #Si la personne ne saisit pas deux mots de même longueur, un message d'erreur apparaît:
        
        return "\nIl est impossible de calculer la distance de hamming car les mots que vous avez saisis ne sont pas de même longueur!"

print(distance_hamming(mot1,mot2))
