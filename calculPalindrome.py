
#On commence d'abord par demander à l'utilisateur de choisir un mot:

motUtilisateur = input("Veuillez saisir un mot pour vérifier s'il s'agit d'un palindrome: ")

motChoisi = motUtilisateur.lower() #On transforme en minuscule le mot de la variable motUtilisateur pour éviter les erreurs liées à la casse.

#Ensuite, on déclare la fonction calculPalindrome():

def calculPalindrome(motChoisi): #On ajoute en paramètres la variable motChoisi, qui contient le mot saisi par l'utilisateur en minuscule

        #Si le mot choisi par l'utilisateur correspond à son inverse, on informe l'utilisateur que le mot est bien un palindrome:
                            
        if motChoisi == motChoisi[::-1]:
        
                return "Le mot \"" + motChoisi + "\" est bien un palindrome"
                
        else: #Sinon, on informe l'utilisateur que le mot n'est pas un palindrome:
                
                return "Le mot \"" + motChoisi + "\" n'est pas un palindrome.\
                        \nDéfinition d'un palindrome: Il s'agit d'un mot qui s’écrit indifféremment de gauche à droite ou de droite à gauche, ce qui n'est pas le cas de \"" + motChoisi + "\"."
        
print(calculPalindrome(motChoisi)) #On affiche le résultat de la fonction calculPalindrome()